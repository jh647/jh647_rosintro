# !/usr/bin/bash
rosservice call /reset
rosservice call /kill "turtle1"
rosservice call /spawn 5.0 7.0 0.0 "J"
rosservice call /spawn 6.0 7.0 0.0 "H"
rosservice call /J/set_pen 255 100 50 5 0
rosservice call /H/set_pen 150 50 100 5 0
rostopic pub -1 /J/cmd_vel geometry_msgs/Twist \-- '[0.0, -3.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /J/cmd_vel geometry_msgs/Twist \-- '[0.0, -4.0, 0.0]' '[0.0, 0.0, -3.0]'
rostopic pub -1 /H/cmd_vel geometry_msgs/Twist \-- '[0.0, -4.25, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /H/cmd_vel geometry_msgs/Twist \-- '[0.0, 2.125, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /H/cmd_vel geometry_msgs/Twist \-- '[2.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /H/cmd_vel geometry_msgs/Twist \-- '[0.0, 2.125, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /H/cmd_vel geometry_msgs/Twist \-- '[0.0, -4.25, 0.0]' '[0.0, 0.0, 0.0]'


