#Robotics Midterm Project
# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

#Initialize moveit_commander and a rospy node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

#Instantiate a RobotCommander Object (provides robot's kinematic model and current joint states)
robot = moveit_commander.RobotCommander()

#Instantiate a Planning Scene Interface (robot's understnading of surrounding world)
scene = moveit_commander.PlanningSceneInterface()

#Instantiate a MoveGroup Commander (an interface to planning group (group of joints)...used to plan and execute motions)
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

#ROS Publisher
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

#Planning to get a joint goal
#starting position for EE
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 8
joint_goal[2] = 0
joint_goal[3] = -tau / 4
joint_goal[4] = 0
joint_goal[5] = tau / 6

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()

#EE Starting position for initial J
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0 #set orientation position
pose_goal.position.x = 0.645112 #starting x position
pose_goal.position.y = 0.4 #starting y postion
pose_goal.position.z = 0.6 #starting z postion
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
joint_goal1 = move_group.get_current_joint_values() #to get the joint values of this pose
print(joint_goal1) #print joint values of this pose

#EE draws straight line down
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.645112 #stays the same for vertical line
pose_goal.position.y = 0.4 #stays the same for vertical line
pose_goal.position.z = 0.4 #decrease by .2 to draw the vertical line downwards 
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
#EE draws horizontal line
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.645112 #stays the same for horizontal line
pose_goal.position.y = 0.2 #decreases by .2 to draw horizontal line to the left
pose_goal.position.z = 0.4 #stays the same for horizontal line
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
#EE draws vertical line up
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.645112 #stays the same for vertical line
pose_goal.position.y = 0.2 #stays the same for vertical line
pose_goal.position.z = 0.5 #increases by .1 to draw the vertical line upwards
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#EE starts initial A by drawing diagonal line up
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.5 #x position to draw diagonal line upwards
pose_goal.position.y = 0.109019 #y position to draw diagonal line upwards
pose_goal.position.z = 0.6 #z position to draw diagonal line upwards
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#EE draws Diagonal Line Down (opposite side of the first diagonal line)
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.3 #x position decreases to draw diagonal line downwards towards opposite side 
pose_goal.position.y = 0.2 #y position stays the same for diagonal line 
pose_goal.position.z = 0.4 #z position decrease to draw diagonal line downwards towards opposite side
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
joint_goal2 = move_group.get_current_joint_values() #to get the joint values of this pose
print(joint_goal2) #print joint values of this pose

#EE goes half way up the diagonal line
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.4 #x postion increases to move up the diagonal line
pose_goal.position.y = 0.109019 #y position stays the same
pose_goal.position.z = 0.55 #z position increases to move up the diagonal line 
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#EE draws horizontal line across
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.55 #.55
pose_goal.position.y = 0.109019
pose_goal.position.z = 0.55
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()

#starting postion for initial H
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.645112
pose_goal.position.y = 0.109019
pose_goal.position.z = 0.6

move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
#draw line down for the first vertical line in the letter H
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.645112
pose_goal.position.y = 0.109019
pose_goal.position.z = 0.4


move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
joint_goal1 = move_group.get_current_joint_values()
print(joint_goal1)

#EE moves up to the center of the vertical line
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.645112
pose_goal.position.y = 0.109019
pose_goal.position.z = 0.5
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
#EE draws horizontal line from the center of the vertical line
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.645112 #stays the same for horizontal line
pose_goal.position.y = 0.4 #increases by .3 to draw horizontal line
pose_goal.position.z = 0.5 #stays the same for horizontal line
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True) #`go()` returns a boolean indicating whether the planning and execution was successful.
move_group.stop() #Calling `stop()` ensures that there is no residual movement
move_group.clear_pose_targets() #clears your targets
joint_goal3 = move_group.get_current_joint_values() #to get the joint values of this pose
print(joint_goal3) #print joint values of this pose

#EE draws half the vertical line downwards
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.645112
pose_goal.position.y = 0.4
pose_goal.position.z = 0.4
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
#EE draws the vertical line upwards to the same height as the first vertical line
pose_goal.orientation.w = 1.0 #orientation position stays the same
pose_goal.position.x = 0.645112
pose_goal.position.y = 0.4
pose_goal.position.z = 0.6
move_group.set_pose_target(pose_goal)
success = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()
