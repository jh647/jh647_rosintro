# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

#Initialize moveit_commander and a rospy node 
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

#Instantiate a RobotCommander Object (provides robot's kinematic model and current joint states)
robot = moveit_commander.RobotCommander()

#Instantiate a Planning Scene Interface (robot's understnading of surrounding world)
scene = moveit_commander.PlanningSceneInterface()

#Instantiate a MoveGroup Commander (an interface to planning group (group of joints)...used to plan and execute motions)
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

#ROS Publisher
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)


def run_pose():
    move_group.set_pose_target(pose_goal) #sets desired pose for the end effector
    success = move_group.go(wait=True) #`go()` returns a boolean indicating whether the planning and execution was successful.
    move_group.stop() #Calling `stop()` ensures that there is no residual movement
    move_group.clear_pose_targets() #clears your targets
    
    
def get_jointvalue():
    joint_goal = move_group.get_current_joint_values() #to get the joint values of this pose
    print(joint_goal) #print joint values of this pose
    
def pose(x,y,z):
    return 
    pose_goal.position.x = x #starting x position
    pose_goal.position.y = y #starting y postion
    pose_goal.position.z = z #starting z postion

#Planning to get a joint goal
#starting position for EE
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 8
joint_goal[3] = -tau / 4
joint_goal[4] = 0
joint_goal[5] = tau / 6

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()

#EE Starting position for initial J
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0 #set orientation position
pose(0.645112,0.4,0.6)
run_pose() #function defined to execute the pose goal
get_jointvalue() #function to get the joint values at this pose

